import r from '../rethinkdb';

const debug = require('debug')('app.dbInit');

const db = 'test';
const user = 'admin';
const host = 'rdb0';
const password = '';

async function dbInit() {
  const conn = await r.connect({ db, host, user, password });
  const list = await r.dbList().promise(conn);
  debug(list);

  debug(await Promise.all(list
      .filter(x => !/rethinkdb/.test(x))
      .map(x => r.dbDrop(x).promise(conn))));

  try {
    debug(await r.dbCreate('test').promise(conn));
  } catch(err) { debug(err); }

  try {
    debug(await r.tableDrop('editor').promise(conn));
  } catch(err) { debug(err); }

  try {
    debug(await r.tableCreate('editor').promise(conn));
  } catch(err) { debug(err); }

  try {
    debug(await r.db('rethinkdb').table('users').delete().promise(conn));
  } catch(err) { debug(err); }

  const grant = async function(id) {
    try {
      debug(await r.db('rethinkdb').table('users')
        .insert({ id, password }).promise(conn));
    } catch(err) { debug(err); }
    try {
      const options = { read: true, write: true, config: true };
      debug(await r.grant(id, options).promise(conn));
    } catch(err) { debug(err); }
  };

  for(let i = 0; i < 10 ; i++) {
    grant(`user${i}@gmail.com`);
  }
  grant('gkatebi@gmail.com');
  grant('alkatebi@yahoo.com');
  grant('anonymous@anonymous.co');

}

dbInit();
