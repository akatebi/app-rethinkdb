import fs from 'fs';
import jwt from 'jsonwebtoken';

const priv = fs.readFileSync(`${__dirname}/../.ssh/id_rsa`);

const debug = require('debug')('app.authorize');

const authorize = ({ jwToken }) => {
  debug('jwToken', jwToken);
  try {
    const verify = jwt.verify(jwToken, priv);
    debug('verify', verify);
    return verify;
  } catch(error) {
    debug('error', error.message);
    return { email: 'anonymous@anonymous.co' };
  }
};

export default authorize;
