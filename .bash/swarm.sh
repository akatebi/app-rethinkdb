#!/bin/bash

# docker build -t akatebi/rethinkdb .
# docker run --network=host --name db -d rethinkdb
# docker run --network=host --name app -d akatebi/rdb

docker-machine create --driver=virtualbox --virtualbox-memory 2048 m0
docker-machine create --driver=virtualbox --virtualbox-memory 2048 w0
docker-machine create --driver=virtualbox --virtualbox-memory 2048 w1
docker-machine create --driver=virtualbox --virtualbox-memory 2048 w2


# initialize swarm
docker swarm init

# create RethinkDB overlay network
docker network create --driver overlay dm-net

# recreate primary with --join flag
docker service create \
  --name rdb-primary \
  --network rdb-net \
  --replicas 1 \
  rethinkdb:latest \
  rethinkdb --bind all \
    --no-http-admin \
    --join rdb-secondary

# create and start rethinkdb secondary
docker service create \
  --name rdb-secondary \
  --network rdb-net \
  --replicas 1 \
  rethinkdb:latest \
  rethinkdb --bind all \
    --no-http-admin \
    --join rdb-primary

# create and start rethinkdb proxy
docker service create \
  --name rdb-proxy \
  --network rdb-net \
  --publish 8080:8080 \
  --publish 28015:28015 \
  rethinkdb:latest rethinkdb proxy \
    --bind all \
    --join rdb-primary

# up 3 nodes (primary + two secondary) to enable automatic failover
docker service scale rdb-secondary=2
# start two rdb-primary instances
docker service scale rdb-primary=1

# docker service rm rdb-proxy
# docker service rm rdb-primary
# docker service rm rdb-secondary
