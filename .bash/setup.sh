#!/bin/bash

# Swarm mode using Docker Machine

source ./config.sh

sudo ifconfig vboxnet0 down
sudo ifconfig vboxnet0 up

# create manager machines
echo "======> Creating $managers manager machines ...";
for node in $(seq 0 $managers);
do
	echo "======> Creating m$node machine ...";
	docker-machine create -d virtualbox m$node;
done

# create worker machines
echo "======> Creating $workers worker machines ...";
for node in $(seq 0 $workers);
do
	echo "======> Creating w$node machine ...";
	docker-machine create -d virtualbox w$node;
done

# list all machines
docker-machine ls

# initialize swarm mode and create a manager
echo "======> Initializing first swarm manager ..."
docker-machine ssh m0 "docker swarm init --listen-addr $(docker-machine ip m0) --advertise-addr $(docker-machine ip m0)"

# get manager and worker tokens
export manager_token=`docker-machine ssh m0 "docker swarm join-token manager -q"`
export worker_token=`docker-machine ssh m0 "docker swarm join-token worker -q"`

echo "manager_token: $manager_token"
echo "worker_token: $worker_token"

other masters join swarm
for node in $(seq 1 $managers);
do
	echo "======> m$node joining swarm as manager ..."
	docker-machine ssh manager$node \
		"docker swarm join \
		--token $manager_token \
		--listen-addr $(docker-machine ip m$node) \
		--advertise-addr $(docker-machine ip m$node) \
		$(docker-machine ip m0)"
done

# show members of swarm
docker-machine ssh m0 "docker node ls"

# workers join swarm
for node in $(seq 0 $workers);
do
	echo "======> w$node joining swarm as worker ..."
	docker-machine ssh w$node \
	"docker swarm join \
	--token $worker_token \
	--listen-addr $(docker-machine ip w$node) \
	--advertise-addr $(docker-machine ip w$node) \
	$(docker-machine ip m0):2377"
done

# show members of swarm
docker-machine ssh m0 "docker node ls"
