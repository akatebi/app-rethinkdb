#!/bin/bash

alias dk=docker
alias dm=docker-machine
bounce() {
  sudo ifconfig vboxnet0 down
  sudo ifconfig vboxnet0 up
}
rmi-none() {
  docker rmi $1 $(docker images | grep "<none>" | awk '{print $3}')
}
dme() {
  if [ $1 ]; then
    eval $(docker-machine env $1)
  else
    unset DOCKER_HOST
    unset DOCKER_MACHINE_NAME
    unset DOCKER_TLS_VERIFY
    unset DOCKER_CERT_PATH
  fi
}
drm() {
  docker rm $(docker ps -aq)
}
drms() {
  docker stop $(docker ps -q)
  drm
}
drmi() {
  docker rmi $(docker images -aq)
}
dmr() {
  dm regenerate-certs $1 -f
}
dmc() {
  dm create -d virtualbox --virtualbox-memory=1024 $1
  dmr $1
}
swarm-init() {
  if [ !$1 ]; then
    ip="192.168.99.100"
  else
    ip=$1
  fi
  docker swarm init --advertise-addr=$ip
}
swarm-join() {
  active=$(dm active)
  echo "active $active"
  token=$(docker swarm join-token worker -q)
  ip=$(dm ip $active)
  echo "token $token"
  dme $1
  docker swarm join --token $token $ip:2377
  dme $active
}
net() {
  docker network create \
  --driver overlay \
  --subnet 10.0.9.0/24 \
  mynet;
}
rdb() {
  docker volume create --name rdb$1
  docker service create --name=rdb$1 \
  --mount type=volume,source=rdb$1,target=/data \
    --network=mynet \
    rethinkdb
    # --publish 8080:8080 \
    # --publish 28015:28015 \
}
api() {
  docker service create --name=api \
    --replicas=1 \
    --network=mynet \
    akatebi/api
    # --publish 4000:4000 \
}
web() {
  docker service create --name=web \
    --replicas=1 \
    --network=mynet \
    --publish 8080:8080 \
    --publish 3000:3000 \
    akatebi/web
}

# set -x
# fcurl() {
  # curl --insecure --cert $DOCKER_CERT_PATH/cert.pem --key $DOCKER_CERT_PATH/key.pem \
    # https://192.168.99.100:2376/images/json | json_pp
# }
