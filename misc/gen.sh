#!/bin/bash

myMocha="mocha -c --require babel-polyfill --compilers js:babel-core/register --check-leaks -t 30000";

mkdir -p json

$myMocha test/accessing-reql.js 2> json/accessing-reql.json
$myMocha test/administration.js 2> json/administration.json
$myMocha test/aggregation.js 2> json/aggregation.json
$myMocha test/backtrace.js 2> json/backtrace.json
$myMocha test/client-backtrace.js 2> json/client-backtrace.json
$myMocha test/config.js 2> json/config.json
$myMocha test/control-structures.js 2> json/control-structures.json
$myMocha test/coverage.js 2> json/coverage.json
$myMocha test/cursor.js 2> json/cursor.json
$myMocha test/dates-and-times.js 2> json/dates-and-times.json
$myMocha test/datum.js 2> json/datum.json
$myMocha test/dequeue.js 2> json/dequeue.json
$myMocha test/document-manipulation.js 2> json/document-manipulation.json
$myMocha test/error.js 2> json/error.json
$myMocha test/extra.js 2> json/extra.json
$myMocha test/geo.js 2> json/geo.json
$myMocha test/joins.js 2> json/joins.json
$myMocha test/manipulating-databases.js 2> json/manipulating-databases.json
$myMocha test/manipulating-tables.js 2> json/manipulating-tables.json
$myMocha test/math-and-logic.js 2> json/math-and-logic.json
$myMocha test/multiple-require.js 2> json/multiple-require.json
$myMocha test/nodeify.js 2> json/nodeify.json
$myMocha test/pool_legacy.js 2> json/pool_legacy.json
$myMocha test/selecting-data.js 2> json/selecting-data.json
$myMocha test/stable.js 2> json/stable.json
$myMocha test/stream.js 2> json/stream.json
$myMocha test/string-manipulation.js 2> json/string-manipulation.json
$myMocha test/tls.js 2> json/tls.json
$myMocha test/transformations.js 2> json/transformations.json
$myMocha test/transform-stream.js 2> json/transform-stream.json
$myMocha test/writable-stream.js 2> json/writable-stream.json
$myMocha test/writing-data.js 2> json/writing-data.json
