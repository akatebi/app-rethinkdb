
function* theMeaningOfLife(x) {
    yield 42 * x;
    yield 142 * x ;
    yield 1142 * x;    
}

var it = theMeaningOfLife(1);

console.log(it.next());
console.log(it.next());

for(var v of theMeaningOfLife(3))
    console.log(v);
