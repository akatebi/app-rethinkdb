import { Observable } from 'rx';

const xs = Observable.interval(100)
    .map(function (x) { return 'first' + x; });

const ys = Observable.interval(100)
    .map(function (x) { return 'second' + x; });

const source = xs.groupJoin(
    ys,
    function () { return Observable.timer(0); },
    function () { return Observable.timer(0); },
    function (x, yy) {
        return yy.select(function (y) {
            return x + y;
        })
    }).mergeAll().take(5);

const subscription = source.subscribe(
    function (x) {
        console.log('Next: ' + x);
    },
    function (err) {
        console.log('Error: ' + err);
    },
    function () {
        console.log('Completed');
    });
