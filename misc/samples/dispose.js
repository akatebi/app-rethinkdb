import { Observable } from 'rx';

const source = Observable.create((observer) => {
  const t1 = setTimeout(() => observer.next(Math.floor(Math.random()*10000000)), 1000);
  const t2 = setTimeout(() => observer.next(Math.floor(Math.random()*10000000)), 2000);
  const t3 = setTimeout(() => observer.next(Math.floor(Math.random()*10000000)), 3000);
  const t4 = setTimeout(() => observer.completed(), 4000);
  return () => {
    console.log('####Dispose');
    clearTimeout(t1);
    clearTimeout(t2);
    clearTimeout(t3);
    // clearTimeout(t4);
  };
});

const subscription = source.subscribe(
  (x) => console.log(x),
  (err) => console.error(err),
  () => console.log('Completed'));

setTimeout(() => subscription.dispose(), 2500);
