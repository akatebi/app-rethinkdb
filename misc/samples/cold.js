import { Observable } from 'rx';

const source = Observable.interval(1000);

const sub1 = source.subscribe(
  (x) => { console.log('Observer 1: onNext: ' + x); },
  (e) => { console.log('Observer 1: onError: ' + e.message); },
  () => { console.log('Observer 1: onCompleted'); });

const sub2 = source.subscribe(
  (x) => { console.log('Observer 2: onNext: ' + x); },
  (e) => { console.log('Observer 2: onError: ' + e.message); },
  () => { console.log('Observer 2: onCompleted'); });

setTimeout(function () {
  sub1.dispose();
  sub2.dispose();
}, 5000);
