import { Observable } from 'rx';

const codes = [
    { keyCode: 38}, // up
    { keyCode: 38}, // up
    { keyCode: 40}, // down
    { keyCode: 40}, // down
    { keyCode: 37}, // left
    { keyCode: 39}, // right
    { keyCode: 37}, // left
    { keyCode: 39}, // right
    { keyCode: 66}, // b
    { keyCode: 65}  // a
];

const source = Observable.from(codes)
    .groupBy(
        function (x) { return x.keyCode; },
        function (x) { return x; });

const subscription = source.subscribe(
    function (obs) {
        // Print the count
        obs.subscribe(function (x) {
            console.log(JSON.stringify(x, 0, 3));
        });
    });
