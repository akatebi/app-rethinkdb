
import net from './net';
// import net from './socket';

import rethinkdb from './ast';

import error from './errors';

rethinkdb.connect = net.connect;

rethinkdb.Error = error;

export default rethinkdb;
