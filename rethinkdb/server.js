import sizeof from 'object-sizeof';
import net from './net';

const debug = require('debug')('app.server');

let queryCount = 0;
let memSize = 0;
// const giga = (s) => Math.round(s * 2 / (1024 * 1024)) / 100;
const byteSend = (token, resp) => {
  memSize += sizeof(resp);
  const { t } = resp;
  if (t === 1 || t === 2) {
    queryCount -= 1;
  }
  // debug('byteSend', token, giga(memSize), queryCount);
};

const connect = (socket, { email }) => {

  const opts = {
    host: 'rdb0',
    user: email,
    password: '',
  };

  net.connect(opts).then(conn => {

    debug('net-connect', conn.getCount());

    socket.emit('net-connect');

    socket.on('token', (id) => {
      const token = conn._getToken();
      socket.emit(`${id}.token`, token);
    });

    socket.on('byteAck', (token, bytes) => {
      memSize -= bytes;
      // debug('byteAck', token, bytes, giga(memSize));
    });

    socket.on('query', (token, query) => {
      queryCount += 1;
      conn.writeQueryQ(token, query, (response) => {
        byteSend(token, response);
        socket.emit(`${token}.query`, response);
      });
    });

    socket.on('more', (token) => {
      conn.writeQueryQ(token, [2], (response) => {
        byteSend(token, response);
        socket.emit(`${token}.query`, response);
      });
    });

    socket.on('stop', (token) => {
      conn.writeQueryQ(token, [3], (response) => {
        // debug(`${token}.stop`, 't=', response.t);
        byteSend(token, response);
        socket.emit(`${token}.query`, response);
      });
    });

    socket.on('net-reconnect', (options = {}, cb) => {
      debug('reconnect', options);
      conn.reconnect(options)
        .then(() => cb(null))
        .catch(error => cb(error));
    });

    socket.on('net-server', cb => {
      // debug('server');
      conn.server()
        .then(server => cb(null, server))
        .catch(error => cb(error));
    });

    socket.on('net-noreplyWait', cb => {
      debug('noreplyWait');
      conn.server()
        .then(() => cb(null))
        .catch(error => cb(error));
    });

    socket.on('disconnect', () => {
      // debug('socket disconnected');
      socket.removeAllListeners();
      socket.conn.removeAllListeners();
      conn.close()
        .then(count => debug('net-close', count))
        .catch(error => debug(error));
    });

  })
  .catch(error => {
    debug('net-error', error);
    socket.emit('net-error', error);
  });

};

export default connect;

// socket.conn.on('flush', (so) => conn.waterMark(so.length));
// socket.conn.on('drain', () => conn.waterMark(ack));
