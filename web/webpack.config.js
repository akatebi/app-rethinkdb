const path = require('path');
const webpack = require('webpack');
const BabiliPlugin = require('babili-webpack-plugin');

process.argv.forEach(arg => {
  if (/^NODE_ENV=/.test(arg)) {
    process.env.NODE_ENV = arg.split('=')[1];
  }
});

const PROD = process.env.NODE_ENV === 'production';
// console.log('===>>>', PROD, process.env.NODE_ENV);

const config = {
  // devtool: false,
  devtool: 'eval',
  // devtool: 'cheap-module-eval-source-map',
  // devtool: PROD ? 'eval' : '#@source-map',
  entry: {
    app: PROD ? [
      './src/index',
    ] : [
      './src/index',
      'webpack-hot-middleware/client',
    ],
    vendor: ['rx', 'babel-polyfill', 'socket.io-client', 'es6-promise', 'whatwg-fetch'],
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
    publicPath: '/dist',
    pathinfo: true,
  },
  // resolve: {
  //   alias: {
  //     react: path.resolve('./node_modules/react'),
  //   },
  // },
  plugins: PROD ? [
    new BabiliPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.EnvironmentPlugin(['NODE_ENV']),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.CommonsChunkPlugin(
      { name: 'vendor', filename: '[name].js' }
    ),
  ] : [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.EnvironmentPlugin(['NODE_ENV']),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.CommonsChunkPlugin(
      { name: 'vendor', filename: '[name].js' }
    ),
  ],
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loader: 'eslint-loader',
        include: path.resolve(__dirname, 'src'),
      },
    ],
    loaders: [
      {
        test: /\.js$/,
        loaders: ['babel-loader'],
        include: [path.resolve(__dirname, 'src'), path.resolve(__dirname, 'rethinkdb')],
        exclude: /node_modules/,
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
        include: path.resolve(__dirname, 'test/data'),
      },
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader'],
        include: path.resolve(__dirname, 'src/scss'),
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        loaders: ['style-loader', 'css-loader'],
        include: path.resolve(__dirname, 'src/scss'),
        exclude: /node_modules/,
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=100000',
        include: [path.resolve(__dirname, 'node_modules'), path.resolve(__dirname, 'src/component/asset')],
      },
    ],
  },
};

module.exports = config;
