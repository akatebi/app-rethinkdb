var webpack = require('webpack');
var path = require('path');

process.env.BABEL_ENV = 'test';

module.exports = {
  devtool: 'inline-source-map',
  plugins: [
    new webpack.DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify('test') })
  ],
  resolve: {
    alias: {
      react: path.resolve('./node_modules/react')
    }
  },
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        include: path.resolve(__dirname, 'spec'),
        loader: 'isparta-instrumenter-loader'
      },
      {
        test: /\.js$/,
        loader: 'eslint-loader',
        include: [
              path.resolve(__dirname, 'spec'),
              path.resolve(__dirname, 'rethinkdb')
            ],
        exclude: [/node_modules/, 'src/protodef.js']
      }
    ],
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [
              path.resolve(__dirname, 'spec'),
              path.resolve(__dirname, 'rethinkdb')
            ],
        exclude: /node_modules/
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
        include: path.resolve(__dirname, 'spec')
      }
    ]
  },
  externals: {
    'cheerio': 'window',
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
    'react/addons': true,
  }

}
