import React, { PropTypes } from 'react';
import { Navbar, NavbarBrand, Nav, NavItem, Button } from 'reactstrap';
import { Link } from 'react-router';
import Scrollbar from './Scrollbar';
import Signin from './Signin';

const NavMenu = props =>
(
  <div className="container-fluid">
    <Navbar color="primary" dark>
      <NavbarBrand tag={Link} to="/">ReQL Web API</NavbarBrand>
      <Nav className="pull-xs-right" navbar>
        <NavItem>
          <Button tag={Link} to="/rethinkdb">
            Admin DB
          </Button>
        </NavItem>
        <NavItem>
          <Button tag={Link} to="/api">
            API
          </Button>
        </NavItem>
        <NavItem>
          <Signin />
        </NavItem>
      </Nav>
    </Navbar>
    <Scrollbar>
      {props.children}
    </Scrollbar>
  </div>
);


NavMenu.propTypes = {
  children: PropTypes.element.isRequired,
};

export default NavMenu;
