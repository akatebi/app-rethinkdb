import React, { Component, PropTypes } from 'react';
import { Button } from 'reactstrap';

const debug = require('debug')('app:facebook');

class FacebookLogin extends Component {

  static propTypes = {
    appId: PropTypes.string.isRequired,
    // autoLoad: PropTypes.bool,
    responseHandler: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.clickHandler = this.clickHandler.bind(this);
    this.checkLoginState = this.checkLoginState.bind(this);
  }

  componentDidMount() {
    const { appId } = this.props;
    window.fbAsyncInit = () => {
      debug('fbAsyncInit');
      window.FB.init({
        appId,
        cookie: true,
        xfbml: true,
        version: 'v2.7',
      });
      window.FB.getLoginStatus(response =>
        this.checkLoginState(response));
    };
    const loadlib = (d, s, id) => {
      if (d.getElementById(id)) {return;}
      debug('loadlib');
      const js = d.createElement(s);
      js.id = id;
      js.src = '//connect.facebook.net/en_US/sdk.js';
      const fjs = d.getElementsByTagName(s)[0];
      fjs.parentNode.insertBefore(js, fjs);
    };
    loadlib(document, 'script', 'facebook-jssdk');
  }

  testAPI ({ authResponse }) {
    debug('Welcome!  Fetching your information....');
    const fields = 'id, first_name, last_name, name, email';
    window.FB.api('/me', { fields }, (me) => {
      debug('me', me);
      this.props.responseHandler(authResponse, me);
    });
  }

  checkLoginState (response) {
    debug('checkLoginStatus', response.status);
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      this.testAPI(response);
      // window.FB.logout();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
    }
  }

  clickHandler () {
    const scope = 'public_profile,email';
    window.FB.login(this.checkLoginState, { scope });
  }

  render() {
    const style = { fontSize: 18, marginRight: 10 };
    const background = '#3b5998';
    const borderColor = background;
    return (
      <Button style={{ background, borderColor }} onClick={this.clickHandler} block>
        <i className="fa fa-facebook-official" style={style} />
        Sign in with Facebook
      </Button>
    );
  }
}

export default FacebookLogin;
