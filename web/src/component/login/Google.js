import React, { PropTypes } from 'react';
import { Button } from 'reactstrap';

const debug = require('debug')('app:google');

export default class GoogleLogin extends React.Component {

  static propTypes = {
    appId: PropTypes.string.isRequired,
    responseHandler: PropTypes.func.isRequired,
  };

  static loadlib(src, callback) {
    const id = 'google-ssdk';
    if (document.getElementById(id)) {
      callback();
      return;
    }
    const d = document;
    const s = 'script';
    const js = d.createElement(s);
    js.type = 'text/javascript';
    js.id = id;
    js.src = src;
    if (callback) js.onload = callback;
    const fjs = d.getElementsByTagName(s)[0];
    fjs.parentNode.insertBefore(js, fjs);
  }

  constructor(props) {
    super(props);
    this.clickHandler = this.clickHandler.bind(this);
  }

  componentDidMount () {
    const src = 'https://apis.google.com/js/api:client.js';
    GoogleLogin.loadlib(src, () =>
      window.gapi.load('auth2', () => {
        const scope = 'profile';
        const { appId } = this.props;
        window.auth2 = window.gapi.auth2.init({
          client_id: appId,
          fetch_basic_profile: true,
          scope,
        });
        const provider = localStorage.getItem('provider');
        debug('google', provider);
        if (provider === 'google') {
          this.clickHandler();
        }
      }));
  }


  clickHandler () {
    const { responseHandler } = this.props;
    // const prompt = 'login consent';
    window.auth2.signIn().then((googleUser) => {
      window.gapi.client.load('plus', 'v1', () => {
        const request = window.gapi.client.plus.people.get({ userId: 'me' });
        request.execute((resp) => {
          debug('Retrieved profile for:', resp);
          responseHandler(googleUser.getAuthResponse(), resp);
        });
      });
    });
  }

  render () {
    const style = { fontSize: 18, marginRight: 10 };
    const background = '#dd4b39';
    const borderColor = background;
    return (
      <Button style={{ background, borderColor }} onClick={this.clickHandler} block>
        <i className="fa fa-google-plus" style={style} />
        Sign in with Google plus
      </Button>
    );
  }
}
