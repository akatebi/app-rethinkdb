import React, { Component, PropTypes } from 'react';

class Scrollbar extends Component {

  static propTypes = {
    children: PropTypes.element.isRequired,
    // className: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.onHandleResize = this.onHandleResize.bind(this);
    this.state = { height: window.innerHeight - 80 };
  }

  componentDidMount() {
    window.addEventListener('resize', this.onHandleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onHandleResize);
  }

  onHandleResize() {
    this.setState({ height: window.innerHeight - 80 });
  }

  render() {

    const style = {
      overflowX: 'hidden',
      overflowY: 'auto',
      height: this.state.height,
    };

    return (
      <div>
        <div style={style}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Scrollbar;
