import React, { Component, PropTypes } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Button } from 'reactstrap';
import { connect } from 'react-redux';
// import decode from 'jwt-decode';
import Google from './login/Google';
import Facebook from './login/Facebook';
import { onUserID } from '../action/signin';

const debug = require('debug')('app:signin');

class Signin extends Component {

  static propTypes = {
    dispatch: PropTypes.func.isRequired,
  };

  static handleErrors(response) {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response;
  }

  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.logout = this.logout.bind(this);
    this.localStorage = this.localStorage.bind(this);
    this.responseGoogle = this.responseGoogle.bind(this);
    this.responseFacebook = this.responseFacebook.bind(this);
    this.state = { dropdownOpen: false };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  login(provider, e) {
    e.preventDefault();
    this.setState({ inflighe: true });
  }

  logout(e) {
    e.preventDefault();
    const { provider } = this.state;
    const jwToken = null;
    const clear = () => this.setState({ jwToken });
    if (provider === 'facebook') {
      window.FB.logout(clear);
    } else if (provider === 'google') {
      window.auth2.disconnect();
      clear();
    }
    this.localStorage(this.state, true);
  }

  responseGoogle(user, me) {
    debug('responseGoogle user', user);
    debug('responseGoogle me', me);
    const headers = new Headers();
    headers.set('Content-Type', 'application/json');
    const method = 'post';
    const userInfo = {
      id: me.id,
      idToken: user.id_token,
      // email: me.emails.find(x => x.type === 'account').value,
      firsName: me.name.givenName,
      accessToken: user.access_token,
    };
    const body = JSON.stringify(userInfo);
    const options = { method, headers, body };
    let inflight = true;
    fetch('/fetch/google', options)
    .then(this.handleErrors)
    .then(resp => resp.json())
    .then((resp) => {
      debug('long term accessToken', resp);
      const { id: userID } = me;
      const image = me.image.url;
      const firstName = me.name.givenName;
      const { jwToken } = resp;
      // debug('jwToken', decode(jwToken));
      inflight = false;
      const provider = 'google';
      this.setState({
        userID,
        image,
        inflight,
        provider,
        firstName,
        jwToken,
      });
      debug('state', this.state);
      this.localStorage(this.state);
    })
    .catch(error => debug('error', error));
  }

  responseFacebook(authResponse, me) {
    debug('responseFacebook authResponse',
      JSON.stringify(authResponse, 0, 2));
    debug('responseFacebook me', me);
    const { userID } = authResponse;
    const headers = new Headers();
    headers.set('Content-Type', 'application/json');
    const method = 'post';
    const body = JSON.stringify({ ...authResponse, ...me, xaccessToken: 'bad' });
    const options = { method, headers, body };
    let inflight = true;
    fetch('/fetch/facebook', options)
    .then(this.handleErrors)
    .then(resp => resp.json())
    .then((resp) => {
      debug('long term accessToken', resp);
      const { jwToken } = resp;
      // debug('jwToken', decode(jwToken));
      const { first_name: firstName } = me;
      inflight = false;
      const image = `//graph.facebook.com/v2.7/${userID}/picture?width=35`;
      const provider = 'facebook';
      this.setState({
        userID,
        image,
        inflight,
        provider,
        firstName,
        jwToken,
      });
      debug('state', this.state);
      this.localStorage(this.state);
    })
    .catch(error => debug('error', error));
  }

  localStorage(state, remove) {
    debug('localStorage', state, remove);
    Object.keys(state)
      .filter(key => key === 'provider' || key === 'jwToken')
      .forEach((key) => {
        if (remove) localStorage.removeItem(key);
        else localStorage.setItem(key, state[key]);
      });
    const { dispatch } = this.props;
    if (remove) {
      dispatch(onUserID('0'));
    } else {
      const { provider, userID } = state;
      dispatch(onUserID(`${provider}-${userID}`));
    }
  }

  render() {
    const login = (
      <DropdownMenu right disabled>
        <DropdownItem tag="a">
          <Google
            appId="673324773353-kka39t3d4pl58ellovfgdvlb82oed463.apps.googleusercontent.com"
            responseHandler={this.responseGoogle}
          />
        </DropdownItem>
        <DropdownItem tag="a">
          <Facebook
            appId="1194538243930180"
            responseHandler={this.responseFacebook}
            autoLoad
          />
        </DropdownItem>
      </DropdownMenu>
    );
    const { provider } = this.state;
    const background = provider === 'facebook' ? '#3b5998' : '#dd4b39';
    const borderColor = background;
    const cursor = 'pointer';
    const borderRadius = 30 / 3;
    const logout = (
      <DropdownMenu right>
        <DropdownItem onClick={this.logout} >
          <Button style={{ background, borderColor }} block>Sign out</Button>
        </DropdownItem>
      </DropdownMenu>
    );
    const { firstName } = this.state;
    return (
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle color="primary" caret disabled={this.state.inflight}>
          {this.state.jwToken ?
            <div style={{ cursor }}><img src={this.state.image} alt="" width="35" style={{ borderRadius }} /> {firstName}</div> :
            'Sign in'}
        </DropdownToggle>
        {this.state.jwToken ? logout : login}
      </Dropdown>
    );
  }
}


export default connect()(Signin);
