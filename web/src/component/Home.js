import React from 'react';
import { Container, Button, Row, Col } from 'reactstrap';
import { Link } from 'react-router';
import Editor from '../container/Editor';

const Home = props =>
(
  <section className="jumbotron text-xs-center m-b-3">
    <Container fluid>
      <Row>
        <Col sm={2} />
        <Col sm={8} >
          <p className="lead">
            <img src="rejs.png" alt="" width="280px" style={{ opacity: 1 }} />
          </p>
          <h1 className="jumbotron-heading display-4">
            RethinkDB API Service
          </h1>
          <p className="lead">
            Providing the stock Rethinkdb Query Language <Link to="/reql-api">API</Link> to your Web and Mobile Apps with a twist. Dropped the database cursor in favor of the <Link to="/reactivex">Reactive Extensions</Link> Observable <a rel="noopener noreferrer" target="_blank" href="https://github.com/Reactive-Extensions/RxJS">RxJS</a>.
            {/* We are using the <Link to="/socketio">Socket.io</Link> library for realtime duplex comunication with our services running on the <a rel="noopener noreferrer" target="_blank" href="http://docker.com">Docker Containers</a>. */}
          </p>
          <p>
            <Button color="primary" tag={Link} to="/getting-started/">
              Getting started
            </Button>
          </p>
          <Editor {...props} />
        </Col>
      </Row>
    </Container>
  </section>
);

export default Home;
