import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import reducer from './reducer';

// Sync dispatched route actions to the history
const createStoreWithMiddleware = applyMiddleware(
  thunkMiddleware,
  createLogger({ predicate: (getState, action) =>
      process.env.NODE_ENV !== 'test' && !/^SPINNER/.test(action.type) && true })
)(createStore);

export default function configureStore(initialState) {
  const store = createStoreWithMiddleware(reducer, initialState);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./reducer', () => {
      const nextRootReducer = reducer;
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}
