import 'es6-promise';
import 'whatwg-fetch';
import React from 'react';
import { Route, IndexRoute } from 'react-router';
import './scss/app.scss';
import NavMenu from './component/NavMenu';
import Home from './component/Home';
import API from './component/API';
import Rethinkdb from './component/Rethinkdb';
import ReQL_API from './component/ReQL-API';
import ReactiveX from './component/ReactiveX';
import Socketio from './component/Socket-io';


export default (
  <Route path="/" component={NavMenu}>
    <IndexRoute component={Home} />
    <Route path="api" component={API} />
    <Route path="rethinkdb" component={Rethinkdb} />
    <Route path="reql-api" component={ReQL_API} />
    <Route path="reactivex" component={ReactiveX} />
    <Route path="socketio" component={Socketio} />
    <Route path="getting-started" component={API} />
  </Route>
);
