import 'babel-polyfill';
import 'es6-promise';
import 'whatwg-fetch';
import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import routes from './routes';
import configureStore from './store';
import './scss/prism';

localStorage.setItem('debug', 'app.*');

const store = configureStore();

render(
  <Provider store={store} >
    <Router history={browserHistory} >
      {routes}
    </Router>
  </Provider>,
  document.getElementById('root')
);
