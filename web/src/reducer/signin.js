import { USER_ID } from '../constant';

const initState = { userID: '0' };

const signin = (state = initState, action) => {
  const { type, userID } = action;
  switch(type) {
    case USER_ID:
      return { ...state, userID };
    default:
      return state;
  }

};

export default signin;
