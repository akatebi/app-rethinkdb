import fs from 'fs';
import serve from 'koa-static';
import koaRouter from 'koa-router';
import bodyparser from 'koa-bodyparser';
import logger from 'koa-logger';
import { facebook_cb, google_cb } from './jwt';
// import { vhostStatic, vhostDynamic } from './vhost';

const router = koaRouter();

export default (koaApp) => {

  const app = koaApp;
  app.use(logger());
  app.use(bodyparser());

  // vhostStatic(app, 's1');

  app.use(serve(`${__dirname}/../assets`));
  app.use(serve(`${__dirname}/../dist`));

  // router.post('/vhost', vhostDynamic(app));

  router.post('/fetch/google', google_cb);
  router.post('/fetch/facebook', facebook_cb);

  // Html5 Routing
  const encoding = 'utf8';
  const readFileThunk = (src) =>
   new Promise((resolve, reject) =>
    fs.readFile(src, { encoding }, (err, data) => {
      if(err) reject(err);
      else resolve(data);
    }));
  router.get('*', function* () {
    const index = `${__dirname}/../assets/index.html`;
    this.body = yield readFileThunk(index);
  });

  app
  .use(router.routes())
  .use(router.allowedMethods());

};
