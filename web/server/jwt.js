import fs from 'fs';
import jwt from 'jsonwebtoken';
import fetch from 'node-fetch';
import querystring from 'querystring';
import { facebook, google } from './provider.json';

const debug = require('debug')('app.jwt');

const priv = fs.readFileSync(`${__dirname}/../.ssh/id_rsa`);
const algorithm = 'HS256';

export const facebook_cb = function* () {
  debug('headers.host', this.request.headers.host);
  debug('facebook', this.request.body);
  const {
      accessToken: fb_exchange_token,
      id,
      email,
  } = this.request.body;
  const grant_type = 'fb_exchange_token';
  const { client_id, client_secret, url } = facebook;
  const query = {
    grant_type,
    client_id,
    client_secret,
    fb_exchange_token,
  };
  const qs = querystring.stringify(query);
  debug(`${url}?${qs}`);
  try {
    const result = yield fetch(`${url}?${qs}`)
      .then(resp => resp.json());
    debug('fetch', JSON.stringify(result, 0, 2));
    if (result.error) throw result.error;
    const expiresIn = 1000;
    const provider = 'facebook';
    const domain = this.request.host;
    debug('domain', domain);
    const jwToken = jwt.sign({ domain, id, email, provider }, priv,
      { algorithm, expiresIn });
    debug('token', jwToken);
    this.body = { jwToken };
  } catch(error) {
    debug(error);
    this.response.status = 401;
    this.response.message = error.message;
  }
};

export const google_cb = function* () {
  debug('headers.host', this.request.headers.host);
  debug('google', this.request.body);
  const { idToken, id } = this.request.body;
  const { clientId, uri } = google;
  const url = `${uri}${idToken}`;
  debug('url', url);
  const headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
  try {
    const result = yield fetch(url, { headers })
      .then(resp => resp.json());
    if (result.error) throw result.error;
    if (result.aud !== clientId) throw new Error('Invalid clientId');
    debug('fetch', JSON.stringify(result, 0, 2));
    const expiresIn = 1000;
    const provider = 'google';
    const domain = this.request.host;
    const { email } = result;
    debug('domain', domain);
    const jwToken = jwt.sign({ domain, id, email, provider }, priv,
      { algorithm, expiresIn });
    this.body = { jwToken };
  } catch(error) {
    debug(error);
    this.request.status = 401;
    this.response.message = error.message;
  }
};
