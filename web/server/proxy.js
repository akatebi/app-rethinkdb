import koa from 'koa';
import proxy from 'koa-proxy';
import fs from 'fs';
import proxy2 from 'koa-http-proxy';

const debug = require('debug')('app.proxy');

const app = koa();
app.use(function* (next) {
  debug('#######', this.url);
  yield next;
});
app.use(proxy({
  host: 'http://rdb0:8080',
}));
app.listen(18888);

//
// Create your proxy server and set the target in the options.
//
const host = 'rdb0';
const port = 8080;
// const key = fs.readFileSync(`${__dirname}/../cert/key.pem`, 'utf8');
// const cert = fs.readFileSync(`${__dirname}/../cert/cert.pem`, 'utf8');
const app2 = koa();

app2.use(proxy2({
  target: {
    host,
    port,
  },
  // secure: true,
  // ssl: {
  //   key,
  //   cert,
  // },
}));

const hostname = 'localhost';
app2.listen(port, (error) => {
  if (error) {
    console.error(error);
  } else {
    console.info('==> ✅  Web Server is listening');
    console.info('==> 🌎  Go to http://%s:%s', hostname, port);
  }
});
