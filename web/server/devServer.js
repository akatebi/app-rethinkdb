/* eslint no-console:0, no-unused-vars:1, func-names:0 */

import 'babel-polyfill';
import koa from 'koa';
import http from 'http';
import './proxy';
import routes from './routes';
import webpackConfig from './webpack';

const app = koa();
const port = 3000;
const hostname = '0.0.0.0';


if (process.env.NODE_ENV === 'development') {
  webpackConfig(app);
}

routes(app);

const httpServer = http.createServer(app.callback());

httpServer.listen(port, hostname, (error) => {
  if (error) {
    console.error(error);
  } else {
    console.info('==> ✅  Web Server is listening');
    console.info('==> 🌎  Go to http://%s:%s', hostname, port);
  }
});
